
# think-wf

ingenious Engine expands think orm

## 介绍
think-wf 工作流组件。think-orm结合Ingenious工作流引擎一个服务组件，为企业提供一套高效、灵活的工作流解决方案。


## 主要特性
- ThinkORM：采用ThinkORM，方便用户进行数据库操作，提高开发效率。
- 丰富的API：提供大量API接口，满足用户在不同场景下的需求，实现流程的灵活管理和控制。
- Ingenious v2引擎：结合Ingenious工作流引擎v2，支持复杂的流程设计和流转，确保流程的稳定性和高效




## 插件依赖环境
- PHP：要求PHP版本为^8.1，确保代码的兼容性和性能。
- php-di/php-di：依赖php-di/php-di库，版本要求为^6.3，用于实现依赖注入，提高代码的可维护性和可扩展性。
- madong/ingenious：依赖madong/ingenious库，版本要求为^2.0，提供工作流引擎的核心功能。

##  链接

---

> 官方：
https://www.madong.tech/

>腾讯频道:
[pd52261144](https://pd.qq.com/s/3edfwx2lm)

>纷传圈子:
[https://pc.fenchuan8.com/#/index?forum=84868&yqm=M9RJ](https://pc.fenchuan8.com/#/index?forum=84868&yqm=M9RJ)


---



``` info
对您有帮助的话，你可以在下方赞助我们，让我们更好的维护开发，谢谢！
特别声明：坚决打击网络诈骗行为，严禁将本插件集成在任何违法违规的程序上。
```

如果对您有帮助，您可以点右上角 💘Star💘支持
