<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\think\wf\model;

use madong\think\wf\basic\BaseModel;
use madong\ingenious\interface\model\IProcessTaskActor;

class ProcessTaskActor extends BaseModel implements IProcessTaskActor
{
    // 数据表主键
    protected $pk = 'id';

    // 表名
    protected $name = 'wf_process_task_actor';


    // 是否自动写入时间戳
    protected $autoWriteTimestamp = true;


    protected $append = ['create_date', 'update_date'];



    // 查询作用域
    public function searchIdAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('id', $value);
        }
    }

    public function searchProcessTaskIdAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('process_task_id', $value);
        }
    }

    public function searchActorIdAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('actor_id', $value);
        }
    }

    /**
     * 流程任务用户-关联任务task
     *
     * @return \think\model\relation\BelongsTo
     */
    public function task(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(ProcessTask::class, 'process_task_id');
    }

    /**
     * 流程用户task-关联实例
     *
     * @return \think\model\relation\HasOneThrough
     */
    public function instance(): \think\model\relation\HasOneThrough
    {
        return $this->hasOneThrough(
            ProcessInstance::class,
            ProcessTask::class,
            'id', // ProcessTask 的外键
            'id', // ProcessInstance 的外键
            'process_task_id', // 当前模型的本地键
            'process_instance_id' // 目标模型的本地键
        );
    }
}
