<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\think\wf\model;

use madong\think\wf\basic\BaseModel;
use madong\ingenious\interface\model\IProcessDefine;

/**
 * 流程定义-模型
 *
 * @author Mr.April
 * @since  1.0
 */
class ProcessDefine extends BaseModel implements IProcessDefine
{

 // 数据表主键
    protected $pk = 'id';

    // 表名
    protected $name = 'wf_process_define';

    // 自动时间戳
    protected $autoWriteTimestamp = true;

    // 自定义时间戳字段
    protected $createTime = 'create_time'; // 自定义创建时间字段
    protected $updateTime = 'update_time'; // 自定义更新时间字段

    // 数据类型转换
    protected $json = ['content'];

    // 追加属性
    protected $append = ['create_date', 'update_date'];


    public function searchNameAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('name', $value);
        }
    }

    public function searchDisplayNameAttr($query, $value)
    {
        if (!empty($value)) {
            $query->whereLike('display_name', '%' . $value . '%');
        }
    }

    public function searchEnabledAttr($query, $value)
    {
        if ($value !== '') {
            $query->where('enabled', $value);
        }
    }

    public function searchDescriptionAttr($query, $value)
    {
        if ($value) {
            $query->whereLike('description', '%' . $value . '%');
        }
    }

    public function searchIsActiveAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('is_active', $value);
        }
    }

    public function searchTypeIdAttr($query, $value)
    {
        if ($value) {
            $query->where('type_id', $value);
        }
    }

    public function searchVersionAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('version', $value);
        }
    }

}
