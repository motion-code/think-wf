<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\think\wf\model;

use madong\think\wf\basic\BaseModel;
use madong\ingenious\interface\model\IProcessTaskActor;

class ProcessTaskActorHistory extends BaseModel implements IProcessTaskActor
{

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $pk = 'id';

    /**
     * 表名
     *
     * @var string
     */
    protected $name = 'wf_process_task_actor_history';


    /**
     * 是否指定时间戳
     *
     * @var bool
     */
    public $autoWriteTimestamp = true;


    protected $append = ['instance_state', 'create_date', 'update_date'];


    /**
     * 定义访问器
     *
     * @return null
     */
    public function getInstanceStateAttr(): mixed
    {
        return $this->instance->state ?? null;
    }

    /**
     * ID-搜索器
     *
     * @param $query
     * @param $value
     */
    public function searchIdAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('id', $value);
        }
    }

    /**
     * 流程任务ID-搜索器
     *
     * @param $query
     * @param $value
     */
    public function searchProcessTaskIdAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('process_task_id', $value);
        }
    }

    /**
     * 参与者ID-搜索器
     *
     * @param $query
     * @param $value
     */
    public function searchActorIdAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('actor_id', $value);
        }
    }

    /**
     * 流程任务用户-关联任务task
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function task()
    {
        return $this->belongsTo(ProcessTaskHistory::class, 'process_task_id');
    }

    public function instance()
    {
        return $this->hasOneThrough(
            ProcessInstanceHistory::class,
            ProcessTaskHistory::class,
            'id', // ProcessTaskHistory 的外键
            'id', // ProcessInstanceHistory 的外键
            'process_task_id', // 当前模型的本地键
            'process_instance_id' // 目标模型的本地键
        );
    }

}
