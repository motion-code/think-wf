<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\think\wf\model;

use madong\ingenious\interface\model\IProcessCcInstance;
use madong\think\wf\basic\BaseModel;

/**
 * 流程实例抄送-模型
 *
 * @author Mr.April
 * @since  1.0
 */
class ProcessCcInstance extends BaseModel implements IProcessCcInstance
{

    // 数据表主键
    protected $pk = 'id';

    // 表名
    protected $name = 'wf_process_cc_instance';

    // 是否自增id
    protected $autoWriteTimestamp = false;

    // 自定义时间戳字段
    protected $createTime = 'create_time'; // 自定义创建时间字段
    protected $updateTime = 'update_time'; // 自定义更新时间字段

    // 追加属性
    protected $append = ['create_date', 'update_date'];


    // 关联 ProcessInstance 模型
    public function instance()
    {
        return $this->belongsTo(ProcessInstance::class, 'process_instance_id', 'id');
    }

    // 通过 ProcessInstance 关联 ProcessDefine 模型
    public function define()
    {
        return $this->hasOneThrough(ProcessDefine::class, ProcessInstance::class, 'id', 'id', 'process_instance_id', 'process_define_id');
    }
}
