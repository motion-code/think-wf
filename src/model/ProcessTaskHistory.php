<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\think\wf\model;

use madong\ingenious\interface\model\IProcessTaskHistory;
use madong\think\wf\basic\BaseModel;

class ProcessTaskHistory extends BaseModel implements IProcessTaskHistory
{

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $pk = 'id';

    /**
     * 表名
     *
     * @var string
     */
    protected $name = 'wf_process_task_history';

    /**
     * 是否指定时间戳
     *
     * @var bool
     */
    public $autoWriteTimestamp = true;

    protected $append = ['create_date', 'update_date', 'expire_date', 'finish_date'];

    protected $json = ['variable'];

    /**
     * 追加更新时间
     *
     * @return string|null
     */
    public function getExpireDateAttr(): ?string
    {
        if ($this->getAttr('expire_time')) {
            try {
                $timestamp = $this->getData('expire_time');
                if (empty($timestamp)) {
                    return null;
                }

                return date('Y-m-d H:i:s', $timestamp);
            } catch (\Exception $e) {
                return null;
            }
        }
        return null;
    }

    /**
     * 完成时间
     *
     * @return string|null
     */
    public function getFinishDateAttr(): ?string
    {
        if ($this->getAttr('finish_time')) {
            try {
                $timestamp = $this->getData('finish_time');
                if (empty($timestamp)) {
                    return null;
                }
                return date('Y-m-d H:i:s', $timestamp);
            } catch (\Exception $e) {
                return null;
            }
        }
        return null;
    }

    /**
     * ID-搜索器
     *
     * @param $query
     * @param $value
     */
    public function searchIdAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('id', $value);
        }
    }

    /**
     * 流程实例ID-搜索器
     *
     * @param $query
     * @param $value
     */
    public function searchProcessInstanceIdAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('process_instance_id', $value);
        }
    }

    /**
     * 任务名称自动识别in搜索器
     *
     * @param $query
     * @param $value
     */
    public function searchTaskNameAttr($query, $value)
    {
        if (!empty($value)) {
            if (is_array($value)) {
                $query->whereIn('task_name', implode(',', $value));
            } else {
                $isTrue = count(explode(',', $value)) > 1;
                if ($isTrue) {
                    $query->whereIn('task_name', $value);
                } else {
                    $query->where('task_name', $value);
                }
            }
        }
    }

    public function searchDisplayNameAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('display_name', $value);
        }
    }

    /**
     * 任务类型-搜索器
     *
     * @param $query
     * @param $value
     */
    public function searchTaskTypeAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('task_type', $value);
        }
    }

    /**
     * 参与类型-搜索器
     *
     * @param $query
     * @param $value
     */
    public function searchPerformTypeAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('perform_type', $value);
        }
    }

    /**
     * 任务状态-搜索器
     *
     * @param $query
     * @param $value
     */
    public function searchTaskStateAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('task_state', $value);
        }
    }

    /**
     * 任务状态自动识别notIn 搜索器
     *
     * @param $query
     * @param $value
     */
    public function searchNotInTaskStateAttr($query, $value)
    {
        if (!empty($value)) {
            if (is_array($value)) {
                $query->whereNotIn('task_state', implode(',', $value));
            } else {
                $isTrue = count(explode(',', $value)) > 1;
                if ($isTrue) {
                    $query->whereNotIn('task_state', explode(',', $value));
                } else {
                    $query->where('task_state', '<>', $value);
                }
            }
        }
    }

    /**
     * 任务处理人
     *
     * @param $query
     * @param $value
     */
    public function searchOperatorAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('operator', $value);
        }
    }

    /**
     * 父任务ID-搜索器
     *
     * @param $query
     * @param $value
     */
    public function searchTaskParentIdAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('task_parent_id', $value);
        }
    }

    public function actors()
    {
        return $this->hasMany(ProcessTaskActorHistory::class, 'process_task_id', 'id');
    }

    /**
     * 关联父级流程实例
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instance()
    {
        return $this->belongsTo(ProcessInstanceHistory::class, 'process_instance_id');
    }

}
