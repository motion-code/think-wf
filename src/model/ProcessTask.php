<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\think\wf\model;

use madong\think\wf\basic\BaseModel;
use madong\ingenious\interface\model\IProcessTask;

class ProcessTask extends BaseModel implements IProcessTask
{
    // 数据表主键
    protected $pk = 'id';

    // 表名
    protected $name = 'wf_process_task';

    // 是否自动写入时间戳
    protected $autoWriteTimestamp = true;

    protected $append = ['create_date', 'update_date', 'expire_date', 'finish_date'];

    protected $json = ['variable']; // 将 JSON 字段转换为 PHP 数组

    // 删除后移除管理的参与人
    public static function onAfterDelete($model)
    {
        $model->actors()->delete();
    }

    public function getExpireDateAttr(): ?string
    {
        $expireTime = $this->getAttr('expire_time');
        if (empty($expireTime) || is_string($expireTime)) {
            return null;
        }
        return date('Y-m-d H:i:s', $expireTime);
    }

    public function getFinishDateAttr(): ?string
    {
        $finishTime = $this->getAttr('finish_time');
        if (empty($finishTime) || is_string($finishTime)) {
            return null;
        }
        return date('Y-m-d H:i:s', $finishTime);
    }

    // 查询作用域
    public function searchIdAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('id', $value);
        }
    }

    public function searchProcessInstanceIdAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('process_instance_id', $value);
        }
    }

    public function searchTaskNameAttr($query, $value)
    {
        if (!empty($value)) {
            if (is_array($value)) {
                $query->whereIn('task_name', $value);
            } else {
                $query->where('task_name', $value);
            }
        }
    }

    public function searchDisplayNameAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('display_name', $value);
        }
    }

    public function searchTaskTypeAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('task_type', $value);
        }
    }

    public function searchPerformTypeAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('perform_type', $value);
        }
    }

    public function searchTaskStateAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('task_state', $value);
        }
    }

    public function searchNotInTaskStateAttr($query, $value)
    {
        if (!empty($value)) {
            if (is_array($value)) {
                $query->whereNotIn('task_state', $value);
            } else {
                $query->where('task_state', '<>', $value);
            }
        }
    }

    public function searchOperatorAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('operator', $value);
        }
    }

    public function searchTaskParentIdAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('task_parent_id', $value);
        }
    }

    public function searchFormKeyAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('form_key', $value);
        }
    }

    public function actors()
    {
        return $this->hasMany(ProcessTaskActor::class, 'process_task_id', 'id');
    }

    public function instance()
    {
        return $this->belongsTo(ProcessInstance::class, 'process_instance_id');
    }
}
