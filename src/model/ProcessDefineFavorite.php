<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\think\wf\model;

use madong\think\wf\basic\BaseModel;
use madong\ingenious\interface\model\IProcessDefineFavorite;

/**
 * 流程定义-模型
 *
 * @author Mr.April
 * @since  1.0
 */
class ProcessDefineFavorite extends BaseModel implements IProcessDefineFavorite
{

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $pk = 'id';

    /**
     * 表名
     *
     * @var string
     */
    protected $name = 'wf_process_define_favorite';

    /**
     * 是否指定时间戳
     *
     * @var bool
     */
    public $autoWriteTimestamp = true;

    protected $append = ['create_date', 'update_date'];

    public function define()
    {
        return $this->belongsTo(ProcessDefine::class, 'process_define_id', 'id');
    }

}
