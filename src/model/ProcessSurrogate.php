<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\think\wf\model;

use madong\ingenious\interface\model\IProcessSurrogate;
use madong\think\wf\basic\BaseModel;

class ProcessSurrogate extends BaseModel implements IProcessSurrogate
{

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $pk = 'id';

    /**
     * 表名
     *
     * @var string
     */
    protected $name = 'wf_process_surrogate';

    /**
     * 是否指定时间戳
     *
     * @var bool
     */
    public $autoWriteTimestamp = true;

    protected $append = ['type_id', 'display_name', 'version', 'create_date', 'update_date', 'start_date', 'end_date'];


    public function getStartDateAttr(): ?string
    {
        if ($this->getAttr('start_time')) {
            try {
                $timestamp = $this->getData('start_time');
                if (empty($timestamp)) {
                    return null;
                }
                return date('Y-m-d H:i:s', $timestamp);
            } catch (\Exception $e) {
                return null;
            }
        }
        return null;
    }

    public function getEndDateAttr(): ?string
    {
        if ($this->getAttr('end_time')) {
            try {
                $timestamp = $this->getData('end_time');
                if (empty($timestamp)) {
                    return null;
                }
                return date('Y-m-d H:i:s', $timestamp);
            } catch (\Exception $e) {
                return null;
            }
        }
        return null;
    }

    /**
     * 定义访问器
     *
     * @return null
     */
    public function getTypeIdAttr(): mixed
    {
        return $this->define ? $this->define->type_id : '';
    }

    /**
     * 定义访问器
     *
     * @return null
     */
    public function getDisplayNameAttr(): mixed
    {
        return $this->define ? $this->define->display_name : '';
    }

    /**
     * 定义访问器
     *
     * @return null
     */
    public function getVersionAttr(): mixed
    {
        return $this->define ? $this->define->version : '';
    }

    /**
     * ID-搜索器
     *
     * @param $query
     * @param $value
     */
    public function serachIdAttr($query, $value)
    {
        if ($value) {
            $query->where('id', $value);
        }
    }

    /**
     * 流程名称-搜索器
     *
     * @param $query
     * @param $value
     */
    public function serachProcessNameAttr($query, $value)
    {
        if ($value) {
            $query->where('process_name', $value);
        }
    }

    /**
     * 授权人-搜索器
     *
     * @param $query
     * @param $value
     */
    public function serachOperatorAttr($query, $value)
    {
        if ($value) {
            $query->where('operator', $value);
        }
    }

    /**
     * 代理人-搜索器
     *
     * @param $query
     * @param $value
     */
    public function serachSurrogateAttr($query, $value)
    {
        if ($value) {
            $query->where('surrogate', $value);
        }
    }

    /**
     * 启用状态-搜索器
     *
     * @param $query
     * @param $value
     */
    public function serachEnabledAttr($query, $value)
    {
        if ($value !== '') {
            $query->where('enabled', $value);
        }
    }

    /**
     * 流程实例一对一流程定义表
     */
    public function define()
    {
        return $this->hasOne(ProcessDefine::class, 'id', 'process_define_id');
    }

}
