<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\think\wf\model;

use madong\ingenious\enums\ProcessConstEnum;
use madong\ingenious\interface\model\IProcessInstanceHistory;
use madong\think\wf\basic\BaseModel;

class ProcessInstanceHistory extends BaseModel implements IProcessInstanceHistory
{
    /**
     * 数据表主键
     *
     * @var string
     */
    protected $pk = 'id';

    /**
     * 表名
     *
     * @var string
     */
    protected $name = 'wf_process_instance_history';



    /**
     * 是否指定时间戳
     *
     * @var bool
     */
    public $autoWriteTimestamp = true;


    protected $append = ['ext', 'form_data','create_date', 'update_date'];

    // 定义字段的数据类型
    protected $json = ['variable'];

    /**
     * 定义访问器
     *
     * @return null
     */
    public function getExtAttr(): mixed
    {
        return $this->variable ? $this->variable : [];
    }

    /**
     * ID-搜索器
     *
     * @param $query
     * @param $value
     */
    public function serachIdAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('id', $value);
        }
    }

    /**
     * 父流程ID-搜索器
     *
     * @param $query
     * @param $value
     */
    public function serachParentIdAttr($query, $value)
    {
        if (!empty($value)) {
            $query->where('parent_id', $value);
        }
    }

    /**
     * 流程定义ID-搜索器
     *
     * @param $query
     * @param $value
     */
    public function serachProcessDefineIdAttr($query, $value)
    {
        if ($value) {
            $query->where('process_define_id', $value);
        }
    }

    /**
     *  实例状态搜索器
     *
     * @param $query
     * @param $value
     */
    public function serachStateAttr($query, $value)
    {
        if ($value) {
            $query->where('state', $value);
        }
    }

    /**
     * 父流程依赖节点-搜索器
     *
     * @param $query
     * @param $value
     */
    public function serachParentNodeNameAttr($query, $value)
    {
        if ($value) {
            $query->where('parent_node_name', $value);
        }
    }

    /**
     * 业务编号-搜索器
     *
     * @param $query
     * @param $value
     */
    public function serachBusinessNoAttr($query, $value)
    {
        if ($value) {
            $query->where('business_no', $value);
        }
    }

    /**
     * 流程发起人-搜索器
     *
     * @param $query
     * @param $value
     */
    public function serachOperatorAttr($query, $value)
    {
        if ($value) {
            $query->where('operator', $value);
        }
    }

    /**
     * 访问器-表单数据
     *
     * @return \stdClass
     */
    public function getFormDataAttr(): \stdClass
    {
        $formData = new \stdClass();
        $ext      = $this->getData('variable');
        if (empty($ext)) {
            return (object)[];
        }
        if (is_string($ext)) {
            $ext = json_decode($ext, true);
        }

        //解决关联输出的时候是stdclass格式转换异常
        if ($ext instanceof \stdClass) {
            $ext = (array)$ext;
        }

        // 过滤以 'f_' 开头的键
        $formDataKeys = array_filter(array_keys($ext), function ($key) {
            return str_starts_with($key, ProcessConstEnum::FORM_DATA_PREFIX->value);
        });
        foreach ($formDataKeys as $key) {
            $formData->{$key} = $ext[$key] ?? '';
        }
        return (object)$formData;
    }

    /**
     * 流程实例-关联父级
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function define()
    {
        return $this->belongsTo(ProcessDefine::class, 'process_define_id', 'id');
    }

}
