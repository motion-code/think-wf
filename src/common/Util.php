<?php
/**
 *+------------------
 * madong
 *+------------------
 * Copyright (c) https://gitee.com/motion-code  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Official Website: http://www.madong.tech
 */

namespace madong\think\wf\common;

use madong\ingenious\enums\ProcessConstEnum;

class Util
{

    /**
     * 将字段数组转换为带前缀的字符串
     * 如果有前缀，则输出为 "前缀字段名" 的形式；后缀格式为 "前缀_字段名"。
     *
     * @param array  $fields 字段数组
     * @param string $prefix 前缀
     *
     * @return string 带前缀的字段字符串
     */
    public static function prefixFields(array $fields, string $prefix = ''): string
    {
        return implode(',', array_map(function ($field) use ($prefix) {
            // 如果有前缀，添加 ' as ' 后缀，后缀格式为 "前缀_字段名"
            return $prefix ? $prefix . $field . ' as ' . str_replace('.', '_', $prefix) . $field : $field;
        }, $fields));
    }

    /**
     * 合并多个以逗号分隔的字段字符串
     *
     * @param array $strings 字符串数组
     *
     * @return string 合并后的字段字符串
     */
    public static function mergeFieldStrings(array $strings): string
    {
        $fields = [];
        foreach ($strings as $string) {
            // 将字符串分割成字段数组
            $fieldArray = explode(',', $string);
            // 将字段添加到唯一字段数组中
            foreach ($fieldArray as $field) {
                $fields[trim($field)] = true; // 使用数组的键来保证唯一性
            }
        }
        return implode(',', array_keys($fields));
    }

    /**
     * 字符串json转数组对象
     *
     * @param string $jsonString
     * @param array  $additionalData
     * @param bool   $toObject
     *
     * @return array|mixed|null
     */
    public static function convertToArrayOrObject(string $jsonString, array $additionalData = [], bool $toObject = false)
    {
        // 将 JSON 字符串转换为数组或对象
        $data = json_decode($jsonString, $toObject);

        // 如果转换为数组，合并其他数据
        if (is_array($data)) {
            return array_merge($data, $additionalData);
        }

        // 如果转换为对象，合并其他数据
        if (is_object($data)) {
            foreach ($additionalData as $key => $value) {
                $data->$key = $value;
            }
            return $data;
        }

        return null; // 如果解析失败，返回 null
    }

    /**
     * 筛选表单数据
     *
     * @param mixed $ext
     *
     * @return \stdClass
     */
    public static function getFormData(mixed $ext): \stdClass
    {
        $formData = new \stdClass();
        if (empty($ext)) {
            return $formData;
        }

        if (is_string($ext)) {
            $ext = json_decode($ext, true);
        }

        if ($ext instanceof \stdClass) {
            $ext = (array)$ext;
        }

        // 过滤以 'f_' 开头的键
        $formDataKeys = array_filter(array_keys($ext), function ($key) {
            return str_starts_with($key, ProcessConstEnum::FORM_DATA_PREFIX->value);
        });
        foreach ($formDataKeys as $key) {
            $formData->{$key} = $ext[$key];
        }
        return $formData;
    }

    /**
     * 获取数据库链接配置
     *
     * @return string|null
     */
    public static function getConnectionConfig(): ?string
    {
        if (function_exists('getWfPluginConfig') && !empty(getWfPluginConfig())) {
            return getWfPluginConfig();
        }
        return null; // 如果条件不满足，返回 null
    }

    /**
     * 返回对应前缀的数据集
     *
     * @param array|object $input
     * @param string       $prefix
     * @param bool         $removePrefix 是否去除前缀
     *
     * @return \madong\think\wf\common\stdClass
     */
    public static function removePrefix(array|object $input, string $prefix, bool $removePrefix = false)
    {
        $result = new \stdClass();
        if (is_array($input)) {
            foreach ($input as $key => $value) {
                if (strpos($key, $prefix) === 0) {
                    if ($removePrefix) {
                        $newKey = substr($key, strlen($prefix));
                    } else {
                        $newKey = $key; // 保留前缀
                    }
                    $result->{$newKey} = $value;
                }
            }
        } elseif (is_object($input)) {
            foreach ($input as $key => $value) {
                if (strpos($key, $prefix) === 0) {
                    if ($removePrefix) {
                        $newKey = substr($key, strlen($prefix));
                    } else {
                        $newKey = $key; // 保留前缀
                    }
                    $result->{$newKey} = $value;
                }
            }
        } else {
            throw new InvalidArgumentException('Input must be an array or an object.');
        }

        return $result;
    }

    /**
     * 将对象或数组中指定键的字符串值转换为对象
     *
     * @param array|object $input 输入数组或对象
     * @param string       $key   指定的键
     *
     * @return array|object
     */
    public static function convertStringToObject($input, string $key)
    {
        if (!is_array($input) && !is_object($input)) {
            throw new InvalidArgumentException('Input must be an array or an object.');
        }

        // 处理对象
        if (is_object($input)) {
            if (property_exists($input, $key) && is_string($input->{$key})) {
                $input->{$key} = json_decode($input->{$key});
            }
        } else if (is_array($input)) {
            if (array_key_exists($key, $input) && is_string($input[$key])) {
                $input[$key] = json_decode($input[$key]);
            }
        }

        return $input;
    }

    /**
     * 去除字符串尾部的指定子字符串
     *
     * @param string $string    原始字符串
     * @param string $substring 要去除的子字符串
     *
     * @return string 去除尾部子字符串后的字符串
     */
    public static function removeTrailingSubstring(string $string, string $substring='.'): string
    {
        if (empty($substring)) {
            return $string;
        }

        if (empty($string)) {
            return '';
        }

        if (substr($string, -strlen($substring)) === $substring) {
            return substr($string, 0, -strlen($substring));
        }
        return $string;
    }
}