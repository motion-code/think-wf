<?php
/**
 *+------------------
 * madong
 *+------------------
 * Copyright (c) https://gitee.com/motion-code  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Official Website: http://www.madong.tech
 */

namespace madong\think\wf\services;

use madong\think\wf\basic\BaseService;
use madong\ingenious\interface\model\IProcessTaskActorHistory;
use madong\ingenious\interface\services\IProcessTaskActorHistoryService;
use madong\ingenious\libs\utils\ArrayHelper;
use madong\ingenious\libs\utils\AssertHelper;
use madong\ingenious\libs\utils\Pagination;
use madong\ingenious\libs\utils\PropertyCopier;
use madong\think\wf\dao\ProcessTaskActorHistoryDao;

class ProcessTaskActorHistoryService extends BaseService implements IProcessTaskActorHistoryService
{

    public function __construct()
    {
        $this->dao = new ProcessTaskActorHistoryDao();
    }

    public function created(object $param): ?IProcessTaskActorHistory
    {
        unset($param->id);
        $model = $this->dao->getModel();
        PropertyCopier::copyProperties($param, $model);
        return $this->dao->save($model->toArray());
    }

    public function updated(object $param): bool
    {
        AssertHelper::notNull($param->id ?? '', '参数ID不能为空');
        $model = $this->dao->get($param->id);
        PropertyCopier::copyProperties($param, $model);
        return $model->save();
    }

    public function findById(string $id): ?IProcessTaskActorHistory
    {
        return $this->dao->get($id, ['*'], ['task', 'instance.define']);
    }

    /**
     * 用户任务-列表
     *
     * @param object $param
     *
     * @return array
     */
    public function list(object $param): array
    {
        $where = ArrayHelper::filterArray(ArrayHelper::paramsFilter($param, [
            ['operator'],
            ['actor_id'],
            ['task_name'],
            ['task_state'],
            ['task_display_name'],
            ['instance_name'],
            ['business_no'],
            ['instance_display_name'],
            ['process_name'],
            ['process_display_name'],
        ]));
        [$page, $limit] = Pagination::getPageValue($param);
        [$items, $total] = $this->dao->getList($where, '*', $page, $limit, '', []);
        return compact('items', 'total');
    }

    /**
     * 创建历史记录
     *
     * @param array $actorArray
     *
     * @return array
     */
    public function createHistoryRecord(array $actorArray): array
    {
        $data = [];
        foreach ($actorArray as $actor) {
            $data[] = $this->dao->save($actor);
        }
        return $data;
    }
}
