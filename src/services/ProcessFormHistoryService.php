<?php
/**
 *+------------------
 * madong
 *+------------------
 * Copyright (c) https://gitee.com/motion-code  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Official Website: http://www.madong.tech
 */

namespace madong\think\wf\services;

use madong\think\wf\basic\BaseService;
use madong\ingenious\enums\ProcessConstEnum;
use madong\ingenious\interface\model\IProcessFormHistory;
use madong\ingenious\interface\services\IProcessFormHistoryService;
use madong\think\wf\dao\ProcessFormHistoryDao;

class ProcessFormHistoryService extends BaseService implements IProcessFormHistoryService
{

    public function __construct()
    {
        $this->dao = new ProcessFormHistoryDao();
    }

    public function created(object $param): ?IProcessFormHistory
    {
        $newVersion = 1.0;//默认版本
        $history    = $this->dao->selectList(['process_form_id' => $param->{ProcessConstEnum::PROCESS_FORM_ID_KEY->value}], 'version', 0, 0, 'version asc', [], true)->last();
        if (!empty($history)) {
            $currentVersion = (float)$history->getData('version');
            $newVersion     = round($currentVersion + 0.1, 1);
        }
        $param->versions    = $newVersion;
        $param->create_time = time();
        unset($param->id);
        return $this->dao->save((array)$param);
    }


    public function findById(string|int $id): ?IProcessFormHistory
    {
        return $this->dao->get($id, ['*'], ['parent', 'define']);
    }
}
