<?php
/**
 *+------------------
 * madong
 *+------------------
 * Copyright (c) https://gitee.com/motion-code  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Official Website: http://www.madong.tech
 */

namespace madong\think\wf\services;

use madong\think\wf\basic\BaseService;
use madong\ingenious\enums\ProcessTaskStateEnum;
use madong\ingenious\ex\LFlowException;
use madong\ingenious\interface\model\IProcessTask;
use madong\ingenious\interface\model\IProcessTaskHistory;
use madong\ingenious\interface\services\IProcessTaskHistoryService;
use madong\ingenious\libs\utils\AssertHelper;
use madong\ingenious\libs\utils\PropertyCopier;
use madong\think\wf\dao\ProcessTaskHistoryDao;

class ProcessTaskHistoryService extends BaseService implements IProcessTaskHistoryService
{

    public function __construct()
    {
        $this->dao = new ProcessTaskHistoryDao();
    }

    public function created(object $param): ?IProcessTaskHistory
    {
        unset($param->id);
        $model = $this->dao->getModel();
        PropertyCopier::copyProperties($param, $model);
        return $this->dao->save($model->toArray());
    }

    public function updated(object $param): bool
    {
        AssertHelper::notNull($param->id ?? '', '参数ID不能为空');
        $model = $this->dao->get($param->id);
        PropertyCopier::copyProperties($param, $model);
        return $model->save();
    }

    public function findById(string|int $id): ?IProcessTaskHistory
    {
        $model = $this->dao->get($id);
        if ($model != null) {
            $model->set('ext', $model->getData('variable'));
            $model->set('variable', json_encode($model->getData('variable')));
        }
        return $model;
    }

    public function list(object $param): array
    {
        try {
            AssertHelper::notNull($param->actor_id ?? '', '[actor_id] - this argument is required; it must not be null');
            $param->task_state              = ProcessTaskStateEnum::FINISHED->value;//追加已完成条件
            $processTaskActorHistoryService = new ProcessTaskActorHistoryService();
            return $processTaskActorHistoryService->list($param);
        } catch (LFlowException $e) {
            return ['items' => [], 'total' => 0];
        }
    }

    public function getDoneTaskList(string|int $processInstanceId, string|array $taskNames): ?array
    {
        $map1 = [
            'process_instance_id' => $processInstanceId,
            'not_in_task_state'   => ProcessTaskStateEnum::DOING->value,
        ];
        if (!empty($taskNames)) {
            $map1['task_name'] = $taskNames;
        }
        return $this->dao->selectList($map1, '*', 0, 0, '', [], true)->all();
    }

    /**
     * 通过任务模型创建历史记录
     *
     * @param \madong\ingenious\interface\model\IProcessTask $taskModel
     */
    public function createHistoryRecord(IProcessTask $taskModel): ?IProcessTaskHistory
    {
        $model = $this->dao->getModel();
        PropertyCopier::copyProperties((object)$taskModel->toArray(), $model);
        return $this->dao->save($model->toArray());
    }

    public function getById(string|int $id): ?IProcessTaskHistory
    {
        return $this->dao->getDetail(['id' => $id]);
    }

}
