<?php
/**
 *+------------------
 * madong
 *+------------------
 * Copyright (c) https://gitee.com/motion-code  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Official Website: http://www.madong.tech
 */

namespace madong\think\wf\services;

use madong\think\wf\basic\BaseService;
use madong\ingenious\interface\model\IProcessCcInstance;
use madong\ingenious\interface\services\IProcessCcInstanceService;
use madong\ingenious\libs\utils\ArrayHelper;
use madong\ingenious\libs\utils\AssertHelper;
use madong\ingenious\libs\utils\Pagination;
use madong\ingenious\libs\utils\PropertyCopier;
use madong\think\wf\dao\ProcessCcInstanceDao;

class ProcessCcInstanceService extends BaseService implements IProcessCcInstanceService
{

    public function __construct()
    {
        $this->dao = new ProcessCcInstanceDao();
    }

    public function created($param): ?IProcessCcInstance
    {
        unset($param->id);
        $model = $this->dao->getModel();
        PropertyCopier::copyProperties($param, $model);
        return $this->dao->save($model->toArray());
    }

    public function batchCreated(array $param): array
    {
        return $this->transaction(function () use ($param) {
            $data = [];
            foreach ($param as $item) {
                $map1 = [
                    'process_instance_id' => $item['process_instance_id'],
                    'actor_id'            => $item['actor_id'],
                    'state'               => 0,
                ];
                $info = $this->dao->get($map1);
                if (!empty($info)) {
                    continue;
                }
                $model  = $this->dao->save($item);
                $data[] = $model->getData($model->getPk());
            }
            return $data;
        });
    }

    public function del(string|array|int $data): array
    {
        return $this->transaction(function () use ($data) {
            $data       = ArrayHelper::normalize($data);
            $deletedIds = [];
            foreach ($data as $i) {
                $item = $this->dao->get($i);
                if (!$item) {
                    continue; // 如果找不到项，跳过
                }
                $item->delete();
                $primaryKey   = $item->getPk();
                $deletedIds[] = $item->{$primaryKey};
            }
            return $deletedIds;
        });
    }

    public function updated($param): bool
    {
        AssertHelper::notNull($param->id ?? '', '参数ID不能为空');
        $model = $this->dao->get($param->id);
        PropertyCopier::copyProperties($param, $model);
        return $model->save();
    }

    public function list($param): array
    {
        $where = ArrayHelper::filterArray(ArrayHelper::paramsFilter($param, [
            ['actor_id', ''],
            ['state', 0],
            ['process_instance_id', ''],
            ['display_name', ''],
            ['name', ''],
            ['business_no'],
            ['operator'],
        ]));
        [$page, $limit] = Pagination::getPageValue($param);
        [$items, $total] = $this->dao->getList($where, ['*'], $page, $limit, '', []);
        return compact('items', 'total');
    }

    public function findById(string $id): ?IProcessCcInstance
    {
        return $this->dao->get($id);
    }
}


