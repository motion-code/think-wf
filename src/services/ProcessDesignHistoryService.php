<?php
/**
 *+------------------
 * madong
 *+------------------
 * Copyright (c) https://gitee.com/motion-code  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Official Website: http://www.madong.tech
 */

namespace madong\think\wf\services;

use madong\think\wf\basic\BaseService;
use madong\think\wf\dao\ProcessDesignHistoryDao;
use madong\ingenious\interface\model\IProcessDesignHistory;
use madong\ingenious\interface\services\IProcessDesignHistoryService;
use madong\ingenious\libs\utils\ArrayHelper;
use madong\ingenious\libs\utils\AssertHelper;
use madong\ingenious\libs\utils\Pagination;
use madong\ingenious\libs\utils\PropertyCopier;


class ProcessDesignHistoryService extends BaseService implements IProcessDesignHistoryService
{

    public function __construct()
    {
        $this->dao = new ProcessDesignHistoryDao();
    }

    public function created(object $param): ?IProcessDesignHistory
    {
        unset($param->id);
        $model = $this->dao->getModel();
        PropertyCopier::copyProperties($param, $model);
        return $this->dao->save($model->toArray());
    }

    public function updated(object $param): bool
    {
        AssertHelper::notNull($param->id ?? '', '参数ID不能为空');
        $processDefine = $this->get($param->id);
        PropertyCopier::copyProperties($param, $processDefine);
        return $processDefine->save();
    }

    public function list(object $param): array
    {
        $where = ArrayHelper::paramsFilter($param, [
            ['id', ''],
            ['process_design_id', ''],
        ]);
        [$page, $limit] = Pagination::getPageValue($param);
        $items = $this->dao->selectList($where, '*', $page, $limit, 'create_time asc', [], true)->toArray();
        $count = $this->dao->count($where,true);
        return compact('items', 'count');
    }

    public function findById(string|int $id): ?IProcessDesignHistory
    {
        AssertHelper::notNull($param->id ?? '', '参数ID不能为空');
        return $this->dao->get($id);
    }

}
