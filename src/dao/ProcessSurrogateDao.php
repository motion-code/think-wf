<?php
/**
 *+------------------
 * madong
 *+------------------
 * Copyright (c) https://gitee.com/motion-code  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Official Website: http://www.madong.tech
 */

namespace madong\think\wf\dao;

use madong\think\wf\basic\BaseDao;
use madong\ingenious\libs\utils\ArrayHelper;
use madong\think\wf\common\Util;
use madong\think\wf\model\ProcessSurrogate;
use madong\think\wf\model\ProcessDefine;
use think\facade\Db;
use madong\helper\DateTime;

class ProcessSurrogateDao extends BaseDao
{

    protected function setModel(): string
    {
        return ProcessSurrogate::class;
    }

    /**
     * 获取委托列表-think-orm 不支持whereHas 所有只能通过连表方式实现 可以使用模型自定义实现
     *
     * @param $where
     * @param $field
     * @param $page
     * @param $limit
     * @param $order
     * @param $with
     *
     * @return array
     */
    public function getList($where = [], $field = ['*'], $page = 0, $limit = 0, $order = '', $with = []): array
    {
        // 定义前缀
        $prefixes = [
            'ps' => 'ps.',
            'pd' => 'pd.',
        ];

        // 组装条件
        $where = $this->buildWhere($where, $prefixes);

        // 定义字段
        $fields = $this->defineFields($prefixes);

        // 获取表名
        $tableNames = $this->getTableNames();

        // 获取表名
        $surrogate = ProcessSurrogate::getTableName();
        $define    = ProcessDefine::getTableName();

        // 获取数据链接
        $connection = Util::getConnectionConfig();

        // 构建查询对象
        $query = $this->buildQuery($tableNames, $fields, $where, $prefixes, $connection);

        //获取总条数
        $total = $query->count();

        // 获取数据并格式化
        $data = $query->page($page, $limit)->select()->toArray();
        return [
            $this->formatResults($data),
            $total,
        ];
    }

    private function getTableNames(): array
    {
        return [
            'surrogate' => ProcessSurrogate::getTableName(),
            'define'    => ProcessDefine::getTableName(),
        ];
    }

    private function buildQuery($tableNames, $fields, $where, $prefixes, $connection = null)
    {
        return Db::connect($connection)
            ->table($tableNames['surrogate'])
            ->alias(explode('.', $prefixes['ps'])[0])
            ->field($fields)
            ->join([$tableNames['define'] => explode('.', $prefixes['pd'])[0]], 'ps.process_define_id = pd.id')
            ->where($where)
            ->order('ps.create_time', 'desc');
    }

    private function buildWhere(array $where, array $prefixes): array
    {
        $smap = ArrayHelper::filterArray(ArrayHelper::paramsFilter($where, [
            ['surrogate', '', '', $prefixes['ps'] . 'surrogate'],
            ['operator', '', '', $prefixes['ps'] . 'operator'],
            ['process_define_id', '', '', $prefixes['ps'] . 'process_define_id'],
            ['enabled', '', '', $prefixes['ps'] . 'enabled'],
        ]));

        $dmap = ArrayHelper::filterArray(ArrayHelper::paramsFilter($where, [
            ['name', '', '', $prefixes['pd'] . 'name'],
            ['display_name', '', '', $prefixes['pd'] . 'display_name'],
        ]));

        return array_merge($smap, $dmap);
    }

    private function defineFields(array $prefixes): string
    {
        $surrogateField = Util::prefixFields([
            'id', 'process_define_id', 'operator', 'surrogate', 'start_time', 'end_time',
            'enabled', 'create_time', 'create_by', 'update_time', 'update_by',
        ], $prefixes['ps']);

        $defineField = Util::prefixFields([
            'id', 'type_id', 'icon', 'name', 'display_name', 'description', 'enabled',
            'is_active', 'content', 'version', 'create_time', 'create_user', 'update_time',
            'update_user', 'delete_time',
        ], $prefixes['pd']);

        return Util::mergeFieldStrings([$surrogateField, $defineField]);
    }

    private function formatResults(array $data): array
    {
        return array_map(function ($item) {
            return [
                'id'                => $item['ps_id'],
                'process_define_id' => $item['ps_process_define_id'],
                'operator'          => $item['ps_operator'],
                'surrogate'         => $item['ps_surrogate'],
                'start_time'        => $item['ps_start_time'],
                'end_time'          => $item['ps_end_time'],
                'enabled'           => $item['ps_enabled'],
                'create_time'       => $item['ps_create_time'],
                'create_by'         => $item['ps_create_by'],
                'update_time'       => $item['ps_update_time'],
                'update_by'         => $item['ps_update_by'],
                'start_date'        => DateTime::timestampToString($item['ps_start_time']),
                'end_date'          => DateTime::timestampToString($item['ps_end_time']),
                'create_date'       => DateTime::timestampToString($item['ps_create_time']),
                'update_date'       => DateTime::timestampToString($item['ps_update_time']),
                'define'            => $this->formatDefine($item),
            ];
        }, $data);
    }

    private function formatDefine(array $item): array
    {
        return [
            'id'           => $item['pd_id'],
            'type_id'      => $item['pd_type_id'],
            'icon'         => $item['pd_icon'],
            'name'         => $item['pd_name'],
            'display_name' => $item['pd_display_name'],
            'description'  => $item['pd_description'],
            'enabled'      => $item['pd_enabled'],
            'is_active'    => $item['pd_is_active'],
            'content'      => Util::convertToArrayOrObject($item['pd_content'], [], true),
            'version'      => $item['pd_version'],
            'create_time'  => $item['pd_create_time'],
            'create_user'  => $item['pd_create_user'],
            'update_time'  => $item['pd_update_time'],
            'update_user'  => $item['pd_update_user'],
            'create_date'  => DateTime::timestampToString($item['pd_create_time']),
            'update_date'  => DateTime::timestampToString($item['pd_update_time']),
        ];
    }
}
