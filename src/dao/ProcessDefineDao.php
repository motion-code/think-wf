<?php
/**
 *+------------------
 * madong
 *+------------------
 * Copyright (c) https://gitee.com/motion-code  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Official Website: http://www.madong.tech
 */

namespace madong\think\wf\dao;

use madong\think\wf\basic\BaseDao;
use madong\think\wf\model\ProcessDefine;

/**
 * @method create($toArray)
 */
class ProcessDefineDao extends BaseDao
{

    protected function setModel(): string
    {
        return ProcessDefine::class;
    }
}
