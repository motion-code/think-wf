<?php

/**
 *+------------------
 * madong
 *+------------------
 * Copyright (c) https://gitee.com/motion-code  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Official Website: http://www.madong.tech
 */

namespace madong\think\wf\basic;

use madong\services\cache\CacheService;
use think\facade\Db;

abstract class BaseService
{

    /**
     * 模型注入
     *
     * @var object
     */
    protected object $dao;

    /**
     * 执行事务
     *
     * @param callable    $closure
     * @param bool        $isTran 是否启用事务
     * @param string|null $framework
     *
     * @return mixed
     */
    public function transaction(callable $closure, bool $isTran = true, ?string $connectionName = null): mixed
    {
        if (empty($connectionName)) {
            //兼容wf插件数据库操作
            if (function_exists('getWfPluginConfig') && !empty(getWfPluginConfig())) {
                $connectionName = getWfPluginConfig();
            }
        }
        return $isTran ? Db::connect($connectionName)->transaction($closure) : $closure();
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->dao, $name], $arguments);
    }
}
