<?php
/**
 *+------------------
 * madong
 *+------------------
 * Copyright (c) https://gitee.com/motion-code  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Official Website: http://www.madong.tech
 */

namespace madong\think\wf\basic;

use madong\helper\Snowflake;
use think\Model;

/**
 * @author Mr.April
 * @since  1.0
 */
class BaseModel extends Model
{

    // 删除时间
//    protected $deleteTime = 'delete_time';
    // 添加时间
    protected $createTime = 'create_time';
    // 更新时间
    protected $updateTime = 'update_time';
    // 隐藏字段
    protected $hidden = ['delete_time'];
    // 只读字段
    protected $readonly = ['created_by', 'create_time'];

    // 时间字段不格式化保留时间截原格式输出
    protected $dateFormat = false;

    private const WORKER_ID = 1;
    private const DATA_CENTER_ID = 1;

    /**
     * 雪花算法实例化类
     *
     * @var Snowflake|null
     */
    private static ?Snowflake $snowflake = null;

    public function __construct(array $data = [])
    {
        $this->initialize();
        parent::__construct($data);

    }

    protected function initialize()
    {
        //兼容wf插件数据库操作
        if (function_exists('getWfPluginConfig') && !empty(getWfPluginConfig())) {
            $this->connection = getWfPluginConfig();
        }
    }

    /**
     * 是否自动写入主键
     *
     * @return int
     */
    protected $autoWriteId = true;

    /**
     * 自动写入主键
     *
     * @return int
     */
    public function autoWriteId()
    {
        return self::generateSnowflakeID();
    }

    /**
     * 获取模型定义的字段列表
     *
     * @return mixed
     */
    public function getFields(): mixed
    {
        return $this->getTableFields();
    }

    /**
     * 获取模型定义的数据库表名【全称】
     *
     * @return string
     */
    public static function getTableName(): string
    {
        $self = new static();
        return $self->getConfig('prefix') . $self->name;
    }

    public function getCreateDateAttr()
    {
        $createTime = $this->getAttr($this->createTime);
        return $createTime ? date('Y-m-d H:i:s', $createTime) : null;
    }

    public function getUpdateDateAttr()
    {
        $updateTime = $this->getAttr($this->updateTime);
        return $updateTime ? date('Y-m-d H:i:s', $updateTime) : null;
    }

    /**
     * 新增事件
     *
     * @param Model $model
     *
     * @return void
     * @throws ApiException
     */
    public static function onBeforeInsert(Model $model): void
    {
        try {
            self::setCreatedBy($model);
        } catch (\Exception $e) {
            throw new MadongException($e->getMessage());
        }
    }

    /**
     * 写入事件
     *
     * @param Model $model
     *
     * @return void
     */
    public static function onBeforeWrite(Model $model): void
    {
        self::setUpdatedBy($model);
    }

    /**
     * 设置创建人
     *
     * @param Model $model
     *
     * @return void
     */
    private static function setCreatedBy(Model $model): void
    {
        $uid = getCurrentUser();
        if ($uid) {
            $model->setAttr('created_by', $uid);
        }
    }

    /**
     * 设置更新人
     *
     * @param Model $model
     *
     * @return void
     */
    private static function setUpdatedBy(Model $model): void
    {
        $uid = getCurrentUser();
        if ($uid) {
            $model->setAttr('updated_by', $uid);
        }
    }

    /**
     * 删除-事件
     *
     * @param \think\Model $model
     */
    public static function onAfterDelete(Model $model)
    {
    }

    /**
     *  实例化雪花算法
     *
     * @return Snowflake
     */
    private static function createSnowflake(): Snowflake
    {
        if (self::$snowflake == null) {
            self::$snowflake = new Snowflake(self::WORKER_ID, self::DATA_CENTER_ID);
        }
        return self::$snowflake;
    }

    /**
     * 生成雪花ID
     *
     * @return int
     */
    private static function generateSnowflakeID(): int
    {
        $snowflake = self::createSnowflake();
        return $snowflake->nextId();
    }

}
